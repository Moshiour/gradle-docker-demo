import { NgModule } from '@angular/core';

import { GradleDockerDemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [GradleDockerDemoSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [GradleDockerDemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class GradleDockerDemoSharedCommonModule {}
